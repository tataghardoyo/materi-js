let angka = [32, 33, 16, 40, 9, 7, 11, 2, 44, 1, 66];
console.log("Sebelumnya : "+angka.join(','));
let ascending = angka.sort();
console.log("Ascending : "+ascending.join(','));
let descending = ascending.reverse();
console.log("Descending : "+descending.join(','));

// let pengurutan = angka.sort(function(a, b){return a - b});
// console.log(pengurutan.join(','));

// let pengurutanTerbalik = angka.sort(function(a, b){return b - a});
// console.log(pengurutanTerbalik.join(','));
