function deretAngka(n){
	var hasilderetAngka = '';
	for(var i=1;i<=n;i++){
		if(i % 3 == 0 && i % 5 == 0){
			hasilderetAngka += 'NIOMIC ';
		} else if(i % 3 == 0) {
			hasilderetAngka += 'NIO ';
		} else if(i % 5 == 0) {
			hasilderetAngka += 'MIC ';
		} else {
			hasilderetAngka += i+' ';
		}
	}
	return hasilderetAngka
}

console.log(deretAngka(10))
console.log(deretAngka(20))
console.log(deretAngka(30))

function deretAngka2(n){
	let hasilderetAngka = '';
	for(var i=1;i<=n;i++){
		hasilderetAngka += cekAngka(i);
	}
	return hasilderetAngka;
}

function cekAngka(angka){

	let KelipatanTiga = angka % 3 == 0;
	if(KelipatanTiga) {
		return 'NIO ';
	}

	let KelipatanLima = angka % 5 == 0;
	if(KelipatanLima) {
		return 'MIC ';
	}

	let KelipatanTigaDanLima = KelipatanTiga && KelipatanLima;
	if(KelipatanTigaDanLima){
		return 'NIOMIC ';
	}

	return angka+' ';
}

console.log(deretAngka2(10));
console.log(deretAngka2(20));
console.log(deretAngka2(30));