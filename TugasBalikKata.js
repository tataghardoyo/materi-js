function balikKata(kata) {
	var newString = "";
	for (var i = kata.length - 1; i >= 0; i--) { 
		newString += kata[i];
	}
	return newString;
}

// testCase
console.log(balikKata("Niomic!"))    
console.log(balikKata("JavaScript"))  
console.log(balikKata("alohahola"))  
console.log(balikKata("Jawa_Barat"))  



function deretAngka(n) {
 var hasilDeretAngka = ''
 for (var i = 1; i <= n; i++) {
   if (i % 3 === 0) {
     hasilDeretAngka += 'NIO '
   } else if (i % 5 === 0) {
     hasilDeretAngka += 'MIC '
   } else {
     hasilDeretAngka += i+' '
   }
 }
 return hasilDeretAngka
}


console.log(deretAngka(10))
console.log(deretAngka(20))
console.log(deretAngka(30))