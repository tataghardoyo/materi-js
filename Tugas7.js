let tinggiBadan = [155,158,180,175,160,161,164,177,156,178];
console.log("perulangan biasa");
for (let index = 0; index < tinggiBadan.length; index++) {
    const element = tinggiBadan[index];
    console.log(element);
}
console.log("perulangan for of");
for (const iterator of tinggiBadan) {
    console.log(iterator);
}
// perulangan for of menghasilkan sintak yang lebih pendek dari perulangan biasa