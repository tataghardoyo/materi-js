let angka = [2, 39, 76, 50, 9, 7, 41, 2, 24, 1, 16];
console.log("Sebelumnya : "+angka.join(','));
let ascending = angka.sort();
console.log("Ascending : "+ascending.join(','));
let descending = ascending.reverse();
console.log("Descending : "+descending.join(','));
let saring = angka.filter((bil) =>{
	return bil > 10;
});
console.log("Filter > 10 : "+saring.join(','));