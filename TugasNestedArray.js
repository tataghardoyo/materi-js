var nestedArray = [
[1,2,3,4],
['Mark Zuckerberg', 'Elon Musk', 'Bill Gates', 'Steve Jobs'],
['Facebook', 'Tesla', 'Microsoft', 'Apple']
]

function panggilNestedArray(original) {
    var copy = [];
    for (var i = 0; i < original.length; ++i) {
        for (var j = 0; j < original[i].length; ++j) {
            if (original[i][j] === undefined) continue;
            if (copy[j] === undefined) copy[j] = [];
            copy[j][i] = original[i][j];
        }
    }
    return copy;
}

console.log(panggilNestedArray(nestedArray));

function transpose(a) {
    return Object.keys(a[0]).map(function(c) {
        return a.map(function(r) { return r[c]; });
    });
}

console.log(transpose(nestedArray));